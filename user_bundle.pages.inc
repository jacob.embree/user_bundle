<?php

/**
 * @file
 * User page callbacks for the user_bundle module.
 */

/**
 * Menu callback; Returns the user register form for a specific user bundle.
 */
function user_bundle_admin($bundle = '') {
  global $user;

  // Grant temporary 'administer users' access (in the scope of this request)
  // for this page because it's hardcoded in user_register_form().
  if (user_access('create ' . $bundle . ' user')) {
    $perm = &drupal_static('user_access');
    $perm[$user->uid]['administer users'] = TRUE;
  }

  return drupal_get_form('user_register_form', $bundle);
}
